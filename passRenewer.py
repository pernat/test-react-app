from pprint import pprint
import yaml
from password_generator import PasswordGenerator


with open("/home/ec2-user/prop.yaml") as f:
    list_doc = yaml.safe_load(f)

# pprint(list_doc)

print(list_doc['author'][0]['yaml'])

pwo = PasswordGenerator()
new_pass = pwo.generate()

list_doc['author'][0]['yaml']['password'] = new_pass


with open("prop.yaml", "w") as f:
    yaml.safe_dump(list_doc, f, default_flow_style=False, sort_keys=False, indent=2)

print("Done")